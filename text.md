# GNU Coreutils commands for editing text

## cat
## tac
## fmt
## more
## less
## pg
## head
## tail
## sort
## uniq
## sed
   Useful for text substitutions. Will be covered in a separate file.

   Gotcha : sed removes newlines at the end of each line before 
   processing the line. Hence 

```bash
sed s/\n// file
```

   will not work.
## grep
   Will be covered in its own page
## tr
   Example
```
tr a b file
```
   will replace all occurrences of the character 'a' in 'file' by 'b'.
