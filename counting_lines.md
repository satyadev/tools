# Counting the number of lines in a file

If the last line of a text file terminates in a newline character,
then

```bash
wc -l filename.txt
```

works.

In any case, the following will work.

```awk
awk 'BEGIN {count=0} {count=count+1} END {print count}' filename.txt
```
