# Grep with Shell variables

I encountered trouble with

```bash

while read -r line
do	
  	grep "$line" file
done
```


I was unable to get $line to work properly with grep.

Eventually, this worked:

```bash

while read -r line
do
	bash -c "grep $line file"
done
```
