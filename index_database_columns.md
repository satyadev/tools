Suppose we produce a csv file of a database with many columns.

Sometimes, we want to extract some of the columns from the csv file.
One easy way to do this is using 
```bash 
cut -d, -f
```
For this, we should know the index of the columns to extract.

An easy way to do this is to extract out the header row, and then
cat with line numbering:
```bash
cat file.csv | head -n1 | tr ',' '\n' | cat -N
```
